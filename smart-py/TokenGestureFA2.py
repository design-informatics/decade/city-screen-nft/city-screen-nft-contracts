import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")

class TokenGesture_FA2(FA2.FA2):
    # Override transfer entry point to disable all transfers
    @sp.entry_point
    def transfer(self, params):
        sp.set_type(params, self.batch_transfer.get_type())
        message = "FA2_TX_DENIED"
        sp.verify(False, message = message)

    pass


    @sp.add_test(name="FA2 tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        FA2_admin = sp.test_account("FA2_admin")
        alice = sp.test_account("Alice")
        bob = sp.test_account("Bob")
        sc.h2("FA2")
        tokenGesture_FA2 = TokenGesture_FA2(
            FA2.FA2_config(single_asset = False, non_fungible = True),
            admin = FA2_admin.address,
            metadata = sp.utils.metadata_of_url("https://example.com")
        )
        sc += tokenGesture_FA2

        sc.h2("Initial minting to Alice")
        test_md = FA2.FA2.make_metadata(
            name     = "Token Gesture FA2",
            decimals = 0,
            symbol   = "TKNGEST" )
        tokenGesture_FA2.mint(
            address  = alice.address,
            token_id = 0,
            amount   = 1,
            metadata = test_md
        ).run(sender = FA2_admin)

        sc.h2("Alice cannot mint her own token")
        test_md = FA2.FA2.make_metadata(
            name     = "Token Gesture FA2",
            decimals = 0,
            symbol   = "TKNGEST" )
        tokenGesture_FA2.mint(
            address  = alice.address,
            token_id = 1,
            amount   = 1,
            metadata = test_md
        ).run(sender = alice, valid = False)

        sc.h2("Alice cannot transfer her own token")
        tokenGesture_FA2.transfer(
            [
                tokenGesture_FA2.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                    amount = 1,
                                                    token_id = 0)
                                        ])
            ]).run(sender = alice, valid = False)

        sc.h2("Admin cannot transfer token from Alice to Bob")
        tokenGesture_FA2.transfer(
            [
                tokenGesture_FA2.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                    amount = 1,
                                                    token_id = 0)
                                        ])
            ]).run(sender = FA2_admin, valid = False)
            


sp.add_compilation_target(
    "TokenGesture_FA2",
    TokenGesture_FA2(
        admin   = sp.address("tz1by1LRYXK7QjPcZH5DaTGCevHvXHDE4gKo"),
        config  = FA2.FA2_config(single_asset = False, non_fungible = True, support_operator = False),
        metadata = sp.utils.metadata_of_url("ipfs://Qmf5fLRZ2X9BBCa2W17aV9RPJ369qYFH17nVQNVWxWY8C9")
    )
)