import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")

class CityScreenNT_FA2(FA2.FA2):
    @sp.entry_point
    def transfer(self, params):
        sp.verify( ~self.is_paused(), message = self.error_message.paused() )
        sp.set_type(params, self.batch_transfer.get_type())
        sp.for transfer in params:
           current_from = transfer.from_
           sp.for tx in transfer.txs:
                if self.config.single_asset:
                    sp.verify(tx.token_id == 0, message = "single-asset: token-id <> 0")

                sender_verify = (self.is_administrator(sp.sender))
                message = "FA2_TX_DENIED"

                if self.config.allow_self_transfer:
                    sender_verify |= (sp.sender == sp.self_address)
                sp.verify(sender_verify, message = message)
                sp.verify(
                    self.data.token_metadata.contains(tx.token_id),
                    message = self.error_message.token_undefined()
                )
                # If amount is 0 we do nothing now:
                sp.if (tx.amount > 0):
                    from_user = self.ledger_key.make(current_from, tx.token_id)
                    sp.verify(
                        (self.data.ledger[from_user].balance >= tx.amount),
                        message = self.error_message.insufficient_balance())
                    to_user = self.ledger_key.make(tx.to_, tx.token_id)
                    self.data.ledger[from_user].balance = sp.as_nat(
                        self.data.ledger[from_user].balance - tx.amount)
                    sp.if self.data.ledger.contains(to_user):
                        self.data.ledger[to_user].balance += tx.amount
                    sp.else:
                         self.data.ledger[to_user] = sp.record(balance = tx.amount)
                sp.else:
                    pass



    pass

    @sp.add_test(name="FA2 tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        FA2_admin = sp.test_account("FA2_admin")
        alice = sp.test_account("Alice")
        bob = sp.test_account("Bob")
        sc.h2("FA2")
        cityScreenNT_FA2 = CityScreenNT_FA2(
            FA2.FA2_config(single_asset = False, non_fungible = True),
            admin = FA2_admin.address,
            metadata = sp.utils.metadata_of_url("https://example.com")
        )
        sc += cityScreenNT_FA2
        sc.h2("Initial minting to Alice")
        test_md = FA2.FA2.make_metadata(
            name     = "City Screen FA2",
            decimals = 0,
            symbol   = "CSNFT" )
        cityScreenNT_FA2.mint(
            address  = alice.address,
            token_id = 0,
            amount   = 1,
            metadata = test_md
        ).run(sender = FA2_admin)
        sc.h2("Alice cannot transfer her own token")
        cityScreenNT_FA2.transfer(
            [
                cityScreenNT_FA2.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                    amount = 1,
                                                    token_id = 0)
                                        ])
            ]).run(sender = alice, valid = False)
        sc.h2("Admin can transfer token from Alice to Bob")
        cityScreenNT_FA2.transfer(
            [
                cityScreenNT_FA2.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                    amount = 1,
                                                    token_id = 0)
                                        ])
            ]).run(sender = FA2_admin)
            


sp.add_compilation_target(
    "CityScreen_FA2_NT_Test",
    CityScreenNT_FA2(
        admin   = sp.address("tz1QwssNLYisRb8ZYbbYsw3BZuKSx2U2RcRo"),
        config  = FA2.FA2_config(single_asset = False, non_fungible = True),
        metadata = sp.big_map({"": sp.utils.bytes_of_string("tezos-storage:content"),"content": sp.utils.bytes_of_string("""{"name" : "City Screen NFT Test"}""")})
    )
)