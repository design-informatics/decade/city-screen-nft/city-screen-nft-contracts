import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")

class CityScreenFA2(FA2.FA2):
    pass

    @sp.add_test(name="FA2 tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        FA2_admin = sp.test_account("FA2_admin")
        sc.h2("FA2")
        cityScreenFA2 = CityScreenFA2(
            FA2.FA2_config(single_asset = False, non_fungible = True),
            admin = FA2_admin.address,
            metadata = sp.utils.metadata_of_url("https://example.com")
        )
        sc += cityScreenFA2
        sc.h2("Initial minting")
        test_md = FA2.FA2.make_metadata(
            name     = "City Screen FA2",
            decimals = 0,
            symbol   = "CSNFT" )
        cityScreenFA2.mint(
            address  = FA2_admin.address,
            token_id = 0,
            amount   = 1,
            metadata = test_md
        ).run(sender = FA2_admin)

sp.add_compilation_target(
    "CityScreen_FA2_Test",
    CityScreenFA2(
        admin   = sp.address("tz1ZM8Q1kURGQEZt3VgVwdATGunk3b3T5iek"),
        config  = FA2.FA2_config(single_asset = False, non_fungible = True),
        metadata = sp.big_map({"": sp.utils.bytes_of_string("tezos-storage:content"),"content": sp.utils.bytes_of_string("""{"name" : "City Screen NFT Test"}""")})
    )
)