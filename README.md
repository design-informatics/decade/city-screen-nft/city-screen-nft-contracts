# City Screen NFT Contracts

Tezos smart contracts

# Useful Resources
https://smartpy.io/docs/guides/FA/FA2
https://opentezos.com/
https://cryptocodeschool.in/tezos/academy

https://wiki.tezosagora.org/build/create-a-tezos-token/nft

## New metadata proposal
https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-21/tzip-21.md